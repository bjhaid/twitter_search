## Twitter search

Command line application that returns count of tweets matching a particular search term in a specificed zip_code with a specified time range.

### Usage instructions

- Create a twitter application at https://apps.twitter.com/app/new
- Go to the *Keys and Access Tokens* tab to get your *Consumer Key (API Key)* and *Consumer Secret (API Secret)*
- Replace `your_consumer_key` in twitter_secret.json with the *Consumer Key (API Key)* and `your_secret_key` with the *Consumer Secret (API Secret)*
- For 32 bits computer run:
`twitter_search_32.exe zip_code radius search_term start_date end_date`
e.g: `twitter_search_32.exe 60601 100 Kobe 2016-02-01 2016-04-15`
- For 64 bits computer run:
`twitter_search_64.exe zip_code radius search_term start_date end_date`
e.g: `twitter_search_64.exe 60601 100 Kobe 2016-02-01 2016-04-15`

Make sure twitter_secret.json is in same path you run above command from
