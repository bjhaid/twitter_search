package main

import "encoding/csv"
import "encoding/json"
import "fmt"
import "io"
import "io/ioutil"
import "net/http"
import "net/url"
import "os"
import "strings"
import "bytes"

type BToken struct {
	TokenType   string `json:"token_type"`
	AccessToken string `json:"access_token"`
}

type TwitterSecret struct {
	ConsumerKey    string `json:"consumer_key"`
	ConsumerSecret string `json:"consumer_secret"`
}

type Tweet struct {
	Statuses []struct {
		Text     string
		Id       int64  `json:"id"`
		Entities string `json:",omitempty"`
	}
	SearchMetadata struct {
		MaxId      int64  `json:"max_id"`
		RefreshUrl string `json:"refresh_url"`
	} `json:"search_metadata"`
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func twitterSecret() TwitterSecret {
	data, err := ioutil.ReadFile("./twitter_secret.json")
	check(err)
	var secret TwitterSecret
	json.Unmarshal(data, &secret)
	if secret.ConsumerKey == "" || secret.ConsumerSecret == "" {
		panic("Please supply a valid consumer_key and consumer_secret in twitter_secret.json")
	}
	return secret
}

func retrieveBearerToken() BToken {
	req, err := http.NewRequest("POST", "https://api.twitter.com/oauth2/token", strings.NewReader("grant_type=client_credentials"))
	check(err)
	secret := twitterSecret()
	req.SetBasicAuth(secret.ConsumerKey, secret.ConsumerSecret)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	client := &http.Client{}
	resp, err := client.Do(req)
	check(err)

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	check(err)

	var bToken BToken
	err = json.Unmarshal(body, &bToken)
	check(err)

	return bToken
}

func geoCode(zipCode string) (string, string) {
	dat, err := Asset("data/zipcode.csv")
	check(err)
	r := csv.NewReader(bytes.NewReader(dat))

	for {
		record, err := r.Read()

		if err == io.EOF {
			break
		}
		check(err)

		if zipCode == record[0] {
			return record[3], record[4]
		}
	}

	fmt.Println("Zip code cannot be found")
	return "", ""
}

func queryTwitter(url, bearerToken string) (Tweet, error) {
	req, err := http.NewRequest("GET", url, nil)
	check(err)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", bearerToken))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	client := &http.Client{}
	resp, err := client.Do(req)
	check(err)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	check(err)
	var tweetSearch Tweet
	err = json.Unmarshal(body, &tweetSearch)

	return tweetSearch, nil
}

func search(zipCode, radius, term, since, until string) int {
	lat, long := geoCode(zipCode)
	if lat == "" && long == "" {
		panic("Zip Code cannot be found")
	}
	term = url.QueryEscape(term)
	bearerToken := retrieveBearerToken().AccessToken
	query := fmt.Sprintf("q=%s&geocode=%s,%s,%smi&since=%s&until=%s&count=100", term, lat, long, radius, since, until)
	twitter_url := fmt.Sprintf("%s%s", "https://api.twitter.com/1.1/search/tweets.json?", query)
	return recursiveSearch(twitter_url, bearerToken, term, lat, long, radius, since, until, 1)
}

func recursiveSearch(twitter_url, bearerToken, term, lat, long, radius, since, until string, count int) int {
	max_id := int64(0)
	tweetSearch, err := queryTwitter(twitter_url, bearerToken)
	check(err)
	status_count := len(tweetSearch.Statuses)
	count = count + status_count - 1
	fmt.Printf("Fetched %v tweets \n", count)

	if (status_count - 1) <= 0 {
		return count
	}

	for _, status := range tweetSearch.Statuses {
		id := status.Id
		if max_id == int64(0) {
			max_id = id
			continue
		}
		if max_id > id {
			max_id = id
		}
	}

	query := fmt.Sprintf("q=%s&geocode=%s,%s,%smi&since=%s&until=%s&max_id=%v&count=100", term, lat, long, radius, since, until, max_id)
	twitter_url = fmt.Sprintf("%s%s", "https://api.twitter.com/1.1/search/tweets.json?", query)

	return recursiveSearch(twitter_url, bearerToken, term, lat, long, radius, since, until, count)
}

func main() {
	if len(os.Args) != 6 {
		fmt.Printf("Usage: \n\n  %s zipcode radius search_term since until\n", os.Args[0])
		os.Exit(1)
	}

	zipCode := os.Args[1]
	radius := os.Args[2]
	term := os.Args[3]
	since := os.Args[4]
	until := os.Args[5]
	count := search(zipCode, radius, term, since, until)

	fmt.Printf("Found %v tweets for %s in zipCode %s within %smiles radius from %s to %s\n", count, term, zipCode, radius, since, until)
}
